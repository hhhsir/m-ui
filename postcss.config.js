module.exports = {
    plugins: {
      autoprefixer: {
        overrideBrowserslist: [
          "Android 4.1",
          "iOS 7.1",
          "Chrome > 48",
          "Safari >= 10",
          "ff >= 18",
          "ie > 11",
          "last 10 versions", // 所有主流浏览器最近10版本用
        ],
      },
    },
  };
  