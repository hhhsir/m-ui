import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
  RouteRecordRaw,
} from "vue-router";
import example from "../markdown/example.md";
import buttonMD from "../markdown/button.md";
import popoverMD from "../markdown/popover.md";
import copyAbleMD from "../markdown/copy-able.md";
import coinAvatarMD from "../markdown/coin-avatar.md";
import Home from "../views/home.vue";
import Layout from "/@/documentation/views/components/Layout.vue";
import { display } from "./modules/display";
import { notice } from "/@/documentation/route/modules/notice";
export const routes: RouteRecordRaw[] = [
  {
    path: "/",
    redirect: "/home",
    name: "Root",
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
  },
  {
    path: "/components",
    component: Layout,
    redirect: "/components/button",
    children: [
      {
        path: "button",
        component: buttonMD,
        meta: {
          title: "按钮",
          category: "base",
        },
      },
      {
        path: "popover",
        component: popoverMD,
        meta: {
          title: "弹出提示",
          category: "base",
        },
      },
      {
        path: "copy-able",
        component: copyAbleMD,
        meta: {
          title: "copy-able",
          category: "business",
        },
      },
      {
        path: "coin-avatar",
        component: coinAvatarMD,
        meta: {
          title: "币种头像",
          category: "coin",
        },
      },
      {
        path: "example",
        component: example,
        meta: {
          title: "路由样例",
          category: "example",
        },
      },
      ...display,
      ...notice,
    ],
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
