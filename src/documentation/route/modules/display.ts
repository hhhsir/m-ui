import {RouteRecordRaw} from "vue-router";
import cellMD from "../../markdown/cell.md";
import countLineMD from "../../markdown/count-line.md";
import emptyMD from "../../markdown/empty.md";
import paginationAMD from "../../markdown/pagination-a.md";
import paginationMD from "../../markdown/pagination.md";
import skeletonMD from "../../markdown/skeleton.md";
export  const display :RouteRecordRaw[]= [
    {
        path:'cell',
        component:cellMD,
        meta:{
            title:"cell",
            category:'display'
        }
    },
    {
        path:'count-line',
        component:countLineMD,
        meta:{
            title:"数量条",
            category:'display'
        }
    },
    {
        path:'empty',
        component:emptyMD,
        meta:{
            title:"空内容",
            category:'display'
        }
    },
    {
        path:'pagination-a',
        component:paginationAMD,
        meta:{
            title:"移动端分页简易版",
            category:'display'
        }
    },
    {
        path:'pagination',
        component:paginationMD,
        meta:{
            title:"移动端分页",
            category:'display'
        }
    },
    {
        path:'skeleton',
        component:skeletonMD,
        meta:{
            title:"骨架屏",
            category:'display'
        }
    },
]