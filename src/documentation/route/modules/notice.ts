import { RouteRecordRaw } from "vue-router";
import messageAMD from "../../markdown/message-a.md";

export const notice: RouteRecordRaw[] = [
  {
    path: "notice",
    component: messageAMD,
    meta: {
      title: "消息通知A",
      category: "notice",
    },
  },
];
