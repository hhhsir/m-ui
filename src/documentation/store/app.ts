import {defineStore} from "pinia";

 export const useAppStore = defineStore('app',{
    state:()=>{
     return {
         isMobile:false,
         isMenuOpen:true,

     }

    },
    actions:{
        setIsMobile(){
            const width = window.innerWidth
            this.isMobile = width <=500
        },
        setIsMenuOpen(flag:boolean){
            this.isMenuOpen = flag
        }
    }

})