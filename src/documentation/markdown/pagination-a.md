# pagination-a


## 基本使用

```vue demo src="../components/pagination-a/base.vue"

```


## API

参数				|说明      | 类型   | 默认值
:----:			|:---   |:---   |:---
value			| 当前页,必传  | `number` | `1`
total			|总条数,必传   | `number` | `0`
limit			|一页几条,必传   | `number` | -
pageSizeOptions  	| 预留字段   | `number` | -
pageSlot | 显示的页码个数 | `number` 奇数 | `7`
size| 尺寸  | `string`  , 目前只支持 `large` 和  `medium`  | large


## 主题

```css
.m-pagination {
  --item-disabled-color:rgba(0, 0, 0, 0.25);
  --item-color: var(--text-color-primary);
  --item-active-color: var(--color-primary);
  --item-size-medium: 20px;
  --item-font-size-medium: var(--text-size-mini);

  --item-size-large: 24px;
  --item-font-size-large: var(--text-size-small);
}
```