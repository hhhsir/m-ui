# coin-avatar

## 基础使用

```vue demo src="../components/coin-avatar/base.vue"

```

## 指定大小

```vue demo src="../components/coin-avatar/size.vue"

```

## API

参数				|说明      | 类型   | 默认值
:----:			|:---   |:---   |:---
coin			|币种  | `CoinName` | -
size |尺寸| `string` | '22px'


## 支持的CoinName

- 'ae',
- 'beam',
- 'bfc',
- 'btm',
- 'cfx',
- 'ckb',
- 'dcr',
- 'ergo',
- 'etc',
- 'eth',
- 'etp',
- 'grin',
- 'hns',
- 'kda',
- 'lbc',
- 'moac',
- 'noicon',
- 'pasc',
- 'pgn',
- 'rvn',
- 'sc',
- 'sero',
- 'trb',
- 'vbk',
- 'vds',
- 'vns',
- 'xmr',
- 'xvg',
- 'zcl'

## 主题变量

```css
.m-coin-avatar {
    --coin-avatar-border-color:var(--border-color-smoke-1);
}
```