# empty

## 基础使用

```vue demo src="../components/empty/base.vue"

```



## 指定文字

```vue demo src="../components/empty/title.vue"

```

## 多语言

```vue demo src="../components/empty/locale.vue"

```

## API

参数				|说明      | 类型   | 默认值
:----:			|:---   |:---   |:---
mainTitle			|上面的标题啊  | `string` | "No Data"
subTitle			|下面的标题  | `string` | -

