# skeleton

## 基础使用

```vue demo src="../components/skeleton/base.vue"

```

## 控制形状 type

```vue demo src="../components/skeleton/type.vue"

```

## 控制大小 size

```vue demo src="../components/skeleton/size.vue"

```

## 控制动画 active

```vue demo src="../components/skeleton/active.vue"

```

## API

参数				|说明      | 类型   | 默认值
:----:			|:---   |:---   |:---
type			| 类型  | `line |  round` | `line`
active			| 是否激活   | `boolean` | `true`
limit			| 尺寸   | `number` |  `16`
width  	| 宽度,只有type是line的时候才生效   |   `number | string` |  `'100%'`