# cell


## 基础使用

```vue demo src="../components/cell/base.vue"

```

## value插槽

```vue demo src="../components/cell/value.vue"

```

## title插槽

```vue demo src="../components/cell/title.vue"

```

## API

参数				|说明      | 类型   | 默认值
:----:			|:---   |:---   |:---
title			|左边的标题  | `string` | -
value			|右边的内容  | `string` | -

## SLOT

参数				|说明
:----:			|:--- 
title			|左边的标题  | 
value			|右边的内容  |  

## 主题

```css
.m-cell {
    --cell-padding-x-size: 24px;
    --cell-padding-y-size: 24px;
    --cell-font-size:12px;
    --cell-line-size:14px;
    --cell-bg-color:var( --color-white );
    --cell-border-color:var( --border-color-smoke-2 );
}
```