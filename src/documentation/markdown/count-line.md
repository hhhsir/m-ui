# count-line

## 基础使用

```vue demo src="../components/count-line/base.vue"

```


## 圆角

```vue demo src="../components/count-line/round.vue"

```

## 方向反转

```vue demo src="../components/count-line/reverse.vue"

```

## API



参数				|说明      | 类型   | 默认值
:----:			|:---   |:---   |:---
color			|颜色数组  | `string[]` | -
count			|数量数组  | `string[]` | -
round			|圆角展示  | `Boolean` | `false`
reverse			|反向展示  | `Boolean` | `false`


## 主题

```css
.m-count-line {
      --count-line-size:6px;
}