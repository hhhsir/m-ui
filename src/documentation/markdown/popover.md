# popover

## 基础使用

```vue demo src="../components/popover/base.vue"

```

## 触发方式 trigger

```vue demo src="../components/popover/trigger.vue"

```

## tooltip宽度 width

```vue demo src="../components/popover/width.vue"

```

## change回调

```vue demo src="../components/popover/change.vue"

```

## tooltip位置 placement

```vue demo src="../components/popover/placement.vue"

```


## API

|   参数   | 说明                      | 类型                                                                                                       | 默认值      |
| :------: | :------------------------ | :--------------------------------------------------------------------------------------------------------- | :---------- |
|   placement   | tooltip的位置     | "top-start","top", "top-end", "right-start" , "right", "right-end", "bottom-start" ,"bottom" ,"bottom-end" , "left-start", "left" , "left-end" | 'top' |
|   trigger   | tooltip的触发方式        | `'hover' ,'click' , 'focus' ,'manual'`                                                                     | `'hover'`  |
|   delay   | 开启弹窗延迟    |            `number`        |         `100`    |
| duration  | 关闭弹窗延迟              | `number`                                                    |  `100`         |
| show | 自定义控制tooltip的显示        | `Boolean`                                         | -           |
|  width   | tooltip的宽度(单位px)          | `string , number`                                                                             |  默认和dom一样宽           |
|  outClose   | 是否开启点击外界自动关闭tooltip           | `Boolean`                    |  `true`         |


## 事件

| 参数  | 说明     | 类型                  | 默认值 |
| :---: | :------- | :-------------------- | :----- |
| change事件 | tooltip变化回调(自定义显示的时候不触发) | (isShow:boolean) => void | -      |

## 插槽


| 参数  | 说明     | 类型                  | 默认值 |
| :---: | :------- | :-------------------- | :----- |
| default | 触发tooltip的元素,只认第一个元素 | -      |
| popover | tooltip里面的元素 | -      |


## css变量

```css

.m-popover {
     --popover-tooltip-bg: rgba(0, 0, 0, 0.75); // tooltip背景颜色
    --popover-tooltip-fc: rgb(236, 234, 234);// tooltip文字颜色
    --popover-arrow-height: 8px;  // tooltip箭头大小,改了可能会挂
    --popover-arrow-offset: 10px; // tooltip箭头偏移
}


```