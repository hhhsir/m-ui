# Button

## 按钮类型 type


支持的类型

- `default`
- `primary`
- `success`
- `warning`
- `danger`
- `info`
- `text`
- `link`

```vue demo src="../components/button/type.vue"

```

## 按钮尺寸 size

支持的尺寸

- `large`
- `medium`
- `small`
- `mini` 还没完善

```vue demo src="../components/button/size.vue"

```

##  圆角按钮 round


```vue demo src="../components/button/round.vue"

```


##  反差按钮 ghost


```vue demo src="../components/button/ghost.vue"

```

##  块级按钮 block


```vue demo src="../components/button/block.vue"

```

##  渐变按钮 gradient


```vue demo src="../components/button/gradient.vue"

```

##  loading 状态 loading

```vue demo src="../components/button/loading.vue"

```


## API

|   参数   | 说明                      | 类型                                                                                                       | 默认值      |
| :------: | :------------------------ | :--------------------------------------------------------------------------------------------------------- | :---------- |
|   type   | 按钮的类型                | `"primary" ,"secondary" , "success" , "warning" , "error" , "danger", "info" , "default" ,"text" , "link"` | `'default'` |
|   size   | 按钮的尺寸                | `'large' , 'medium' , 'small', 'mini'`                                                                     | `'medium'`  |
|   icon   | 预留字段,代码还没写       |                                                                                                            |             |
| loading  | loading 状态              | `Boolean`                                                                                                  | -           |
| disabled | 是否警用                  | `Boolean`                                                                                                  | -           |
|  round   | 原边按钮                  | `Boolean`                                                                                                  | -           |
|  block   | 块级按钮                  | `Boolean`                                                                                                  | -           |
|  ghost   | 幽灵按钮                  | `Boolean`                                                                                                  | -           |
| gradient | 渐变按钮,可以当背景色使用 | `Boolean,String`                                                                                           | `false`     |

## 事件

| 参数  | 说明     | 类型                  | 默认值 |
| :---: | :------- | :-------------------- | :----- |
| click | 点击时间 | (event:Event) => void | -      |

## 主题

```css
.m-button {
  --MuiButton-SpaceYSize-Mini: 0;
  --MuiButton-SpaceXSize-Mini: 10px;
  --MuiButton-SpaceYSize-Small: 3px;
  --MuiButton-SpaceXSize-Small: 15px;
  --MuiButton-SpaceYSize-Medium: var(--space-size-mini);
  --MuiButton-SpaceXSize-Medium: var(--space-size-large);

  --MuiButton-SpaceYSize-Large: 6.4px;
  --MuiButton-SpaceXSize-Large: 15px;
  --MuiButton-Height-Mini: 24px;
  --MuiButton-Height-Small: 28px;
  --MuiButton-Height-Medium: 32px;
  --MuiButton-Height-Large: 40px;

  --MuiButton-BorderRadiusSize-Medium: var(--border-radius-size-Small);
  --MuiButton-BorderRadiusSize-Large: var(--border-radius-size-medium);
}
```

```css
:root {
  --color-white: #ffffff;
  --color-black: #000000;
  --color-success: #00d97d;
  --color-warning: #f6c343;
  --color-danger: #e63757;
  --color-info: #6d83a3;
  --color-disabled: #f5f5f5;
}
```

