# copy-able

## 基础使用

```vue demo src="../components/copy-able/base.vue"

```

## 分别指定展示和复制的字符串

```vue demo src="../components/copy-able/showText.vue"

```

## API

|   参数   | 说明                                      | 类型     | 默认值 |
| :------: | :---------------------------------------- | :------- | :----- |
| showText | 左侧展示的字符串,没有的话将使用`copyText` | `string` | -      |
| copyText | 复制的字符串,没有的话将使用 `showText`     | `string` | -      |

## 事件

| 参数 | 说明                             | 类型                   | 默认值 |
| :--: | :------------------------------- | :--------------------- | :----- |
| copy | 复制完成的回调,flag:复制是否成功 | (flag:Boolean) => void | -      |

## 主题变量

```css
.m-copy-able {
    --copy-able-icon-color:var(--text-color-info);
    --copy-able-icon-size:var(--text-size-medium);
    --copy-able-text-color:var(--text-color-info);
    --copy-able-text-size:var(--text-size-mini);
}

```