# 消息通知

- 因为感觉会有messageB,所以先做messageA

## 基础使用

```vue demo src="../components/message-a/base.vue"

```

## 消息类型 type

```vue demo src="../components/message-a/type.vue"

```

## 消息位置 offset

```vue demo src="../components/message-a/offset.vue"

```

## 停留时间 durantion

```vue demo src="../components/message-a/durantion.vue"

```

## 关闭回调 onClose

```vue demo src="../components/message-a/on-close.vue"

```

## 组件单独使用

- MMessageA组件接受的参数和messageA函数的参数基本一致


```vue demo src="../components/message-a/MMessageA.vue"

```


## API

参数				|说明      | 类型   | 默认值
:----:			|:---   |:---   |:---
type			|类型  | ` "","primary", "success","warning", "danger", "info"` | ''
message			|显示的文字  | `string` | 必填
durantion			|停留时间  | `number` | 2000
center			| 文字是否居中  | `boolean` | `true`
onClose			| 关闭回调  | `function` | -
offset			| 位置,偏移量  | `number` | 第一个是20,后面的会累加
zIndex			| 层级  | `number` | `999`
show			| 手动控制是否显示,函数调用message时候勿传  | `boolean` | -


## 主题

```css
.m-messageA {
     font-size: var(--text-size-small);
    padding:  var( --space-size-default);
    border-radius: var(--border-radius-size-medium);

     background: var(--color-[type]);
}

