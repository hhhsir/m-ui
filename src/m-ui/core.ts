import type {App} from "vue";
import {LocaleInjectionKey, localeProviderMaker} from "./hooks";
import {Language} from "./hooks/useLocale";

interface MuiOptions {
    components?:Record<string, any>,
    locale?:Language
}

export default class MUI {
    static instance = null
    static vueInstance: App = null // Needed until constructor is called.
    static  registered=false

    constructor(app:App,options: MuiOptions = {}) {
        if (MUI.instance) {

            return MUI.instance
        } else {
            if(!MUI.registered){
                app.use(MUI,options)
            }

            MUI.instance = this;


        }
    }

    /**
     * tree-shaking使用,
     * @param app
     * @param options
     */

    static install(app: App, options: MuiOptions = {}) {
        // Register a-la-carte components from the given list.
        const {components = {}} = options || {};
        for (const id in components) {

            const component = components[id];
            app.component(component.name, component);
        }
        if (options.locale) {
            const localeProvides = localeProviderMaker(options.locale);
            app.provide(LocaleInjectionKey, localeProvides);
        }

        MUI.vueInstance = app;
        MUI.registered = true;
    }
}
