import MUI from './core'
import * as components from './components'
import {messageA} from './components'
const install = MUI.install
MUI.install = (app,options={})=>{
    return install.call(MUI,app,{
        components,
        ...options
    })
}

export  {
    messageA
}

export default  MUI