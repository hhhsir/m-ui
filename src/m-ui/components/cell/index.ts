import { App } from "vue";
import MCell from "./src/index.vue";

MCell.install = (app: App): void => {
  app.component(MCell.name, MCell);
};
export default MCell;
