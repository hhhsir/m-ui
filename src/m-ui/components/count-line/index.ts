import { App } from "vue";
import MCountLine from "./src/MCountLine.vue";
import MCountLineItem from "./src/MCountLineItem.vue";
MCountLine.install = (app): void => {
  app.component(MCountLine.name, MCountLine);
};

MCountLineItem.install = (app): void => {
  app.component(MCountLineItem.name, MCountLineItem);
};

export { MCountLine, MCountLineItem };
