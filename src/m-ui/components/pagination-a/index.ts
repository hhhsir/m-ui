import { App } from "vue";
import MPaginationA from "./src/index.vue";

MPaginationA.install = (app: App): void => {
  app.component(MPaginationA.name, MPaginationA);
};
export default MPaginationA;
