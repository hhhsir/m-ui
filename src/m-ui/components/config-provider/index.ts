import { App } from "vue";
import MConfigProvider from "./config-provider.vue";

MConfigProvider.install = (app: App): void => {
  app.component(MConfigProvider.name, MConfigProvider);
};
export default MConfigProvider;
