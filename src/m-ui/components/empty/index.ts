import { App } from "vue";
import MEmpty from "./src/index.vue";

MEmpty.install = (app: App): void => {
  app.component(MEmpty.name, MEmpty);
};
export default MEmpty;
