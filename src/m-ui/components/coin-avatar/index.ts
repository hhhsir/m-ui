import { App } from "vue";
import MCoinAvatar from "./src/index.vue";

MCoinAvatar.install = (app: App): void => {
  app.component(MCoinAvatar.name, MCoinAvatar);
};
export default MCoinAvatar;
