export type MessageType =
  | "primary"
  | "success"
  | "warning"
  | "danger"
  | "info";

export interface messageAProps {
  type?: MessageType;
  message: string;
  durantion?:number;
  center?:boolean;
  onClose?:()=>void;
  offset?:number;
}




