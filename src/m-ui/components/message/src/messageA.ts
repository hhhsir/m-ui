import useCreateAPI from "../../../hooks/useCreateApi";
import { messageAProps } from "./messageA.type";

const instances: HTMLElement[] = [];
let zIndex = 999;

export function messageA(props: messageAProps | string) {
  if (typeof props === "string") {
    props = {
      message: props,
      onClose: () => {},
    };
  }

  //   计算偏移量
  let offset = props.offset || 20;

  instances.forEach((element) => {
    offset += element.offsetHeight + 20;
  });

  const userClose = props.onClose;

  const opts = {
    offset,
    zIndex: zIndex++,
    ...props,
    show: undefined,
    onClose: () => {
      instances.pop();
      userClose && userClose();
    },
  };
  //   console.log(opts)

  import("./MessageA.vue").then((MessageA) => {
    const { el } = useCreateAPI(MessageA.default, opts);
    instances.push(el as HTMLElement);
  });
}
