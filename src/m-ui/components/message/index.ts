import { messageA } from "./src/messageA";
import MMessageA from "./src/MessageA.vue";

MMessageA.install = function (app) {
  app.component(MMessageA.name, MMessageA);
};

export { messageA, MMessageA };
