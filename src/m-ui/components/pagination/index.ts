import { App } from "vue";
import MPagination from "./src/index.vue";

MPagination.install = (app: App): void => {
  app.component(MPagination.name, MPagination);
};
export default MPagination;
