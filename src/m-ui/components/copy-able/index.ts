import { App } from "vue";
import MCopyAble from "./src/index.vue";

MCopyAble.install = (app: App): void => {
  app.component(MCopyAble.name, MCopyAble);
};
export default MCopyAble;
