import MPopover from "./src/index.vue";

MPopover.install = (app): void => {
  app.component(MPopover.name, MPopover);
};

export default MPopover;
