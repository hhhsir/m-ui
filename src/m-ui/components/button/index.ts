import MButton from "./src/index.vue";

import {App} from "vue";
MButton.install = (app: App): void => {
    app.component(MButton.name, MButton);
};

export default MButton;
