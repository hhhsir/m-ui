import { App } from "vue";
import MSkeleton from "./src/MSkeleton.vue";

MSkeleton.install = (app: App): void => {
  app.component(MSkeleton.name, MSkeleton);
};

export default MSkeleton;
