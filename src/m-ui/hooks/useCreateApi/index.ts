import { createVNode, render } from "vue";
export default function useCreateAPI(
  component,
  componentProps = {},
  host = document.body

) {
  const divDom = document.createElement("div");
  //  instance = vNode
  const instance = createVNode(component, componentProps);

  // 挂载
  render(instance, divDom);
  // el = 真实dom
  const el = divDom.firstElementChild;

  host.appendChild(el);

  instance.props.onDestrory = () => {
    render(null, divDom);
  };

  return {
    el,
  };
}
