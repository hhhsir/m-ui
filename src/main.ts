import { createApp } from "vue";
import App from "./App.vue";
import MUI from "/@/m-ui";
console.log(MUI);
// import {
//     MPaginationA,
//     MEmpty,
//     MPagination,
//     MButton,
//     MCell,
//     MCoinAvatar,
//     MConfigProvider,
//     MCopyAble,
//     MCountLine,
//     MCountLineItem,
//     MMessageA,
//     MPopover,
//     MSkeleton,
// } from './m-ui/components'
import "./m-ui/scss/index.scss";
const app = createApp(App);
import "./documentation/scss/normalize.css";
import "vite-plugin-vuedoc/style.css";
import "./documentation/scss/index.scss";
import router from "./documentation/route/index";
import { createPinia } from "pinia";
// app.use(MUI);

new MUI(app,)

app.use(router);
app.use(createPinia());
// app.use(MUI);
app.mount("#app");
