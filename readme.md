

> vue3组件库
> 一些业务组件,少量基础组件


[online document](https://m-ui-8gu40626d678e3fb-1258857408.ap-shanghai.app.tcloudbase.com/)


## 安装


```shell
yarn add @minerui/m-ui
# 1.x需要安装额外样式库,2.x不需要
yarn add @minerui/mui-theme-chalk
```

## 样式引入


或者

```shell
# 1.x
import '@minerui/m-ui/dist/theme-chalk/index.css'
# 2.x
import '@minerui/mui-theme-chalk'
```



## 组件引入

- 1.x
- 
```ts
import { createApp } from 'vue'
import App from './App.vue'
import '@minerui/mui-theme-chalk/lib/index.css'

import MUI from '@minerui/m-ui/lib/m-ui'
createApp(App).use(MUI).mount('#app')
```

- 2.x

```ts
import { createApp } from 'vue'
import App from './App.vue'
import MUI from '@minerui/m-ui'
import '@minerui/m-ui/dist/theme-chalk/index.css'
createApp(App).use(MUI).mount('#app')
```

## 2.x不再支持单个组件引入

```ts
// 这种写法将不再支持
import {MCoinAvatar} from '@minerui/m-ui'
```

## tree-shaking

尚未开发

## 主题

###### 全局主题变量,一般是一些主题色

```css
:root {
    /* 颜色系列 */
--color-primary: #409eff;
--color-white: #ffffff;
--color-black: #000000;
--color-success: #00d97d;
--color-warning: #f6c343;
--color-danger: #e63757;
--color-info: #6d83a3;
--color-online: #2d7be5;
--color-stop: #12263f;
/* 边框颜色系列 */
--border-color-smoke-1: #e2eaf6;
--border-color-smoke-2: #e3ebf6;
--border-color-smoke-3: #edf2f9;

/* 字体颜色系列 */
--text-color-primary: #12273f;
--text-color-main: #283e59;
--text-color-info: #95a9c9;

/* 字体大小系列 */
--text-size-mini: 12px;
--text-size-small: 14px;
--text-size-medium: 16px;
--text-size-large: 18px;
/* 间距系列 */
 --space-size-mini: 4px;
}

```

##### 一些默认设置

```css
:root {
  font-size: var(--text-size-mini);
  box-sizing: border-box;
  color: var(--text-color-main);
}

*,
::before,
::after {
  box-sizing: inherit;
}
```

#### 覆盖变量

- 直接覆盖

```css
:root { 
    --color-primary: red;
}
```

- 组件作用域下的变量覆盖

```css
.m-coin-avatar {
    --coin-avatar-border-color:red;
}

```

- 如果不想影响到别的地方的同一个组件,加上scope

```vue
<style scoped>
.m-coin-avatar {
  --coin-avatar-border-color:red;
}

</style>
```

#### 仓库

> https://gitee.com/hhhsir/m-ui