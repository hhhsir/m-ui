import typescript from "rollup-plugin-typescript2";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import vue from "rollup-plugin-vue";
import path from "path";
import fs from "fs";

const packagesDir = path.resolve(__dirname, "../src/m-ui/components/");

const target = fs
  .readdirSync(packagesDir)
  .filter(Boolean)
  .filter((name) => {
    const isDirectory =  fs.statSync(path.resolve(packagesDir, name)).isDirectory()
    return isDirectory
  })
  // .filter((name) => {
  //   return name !== "index.ts"
  // });



export default target.map((name) => {
  const pckName = name;
  return {
    input: path.resolve(__dirname, `../src/m-ui/components/${pckName}/index.ts`),
    output: {
      format: "es",
      file: path.resolve(__dirname, `../dist/es/${pckName}/index.js`)
      // `lib/${pckName}/index.js`,
    },
    plugins: [
      nodeResolve(),


      typescript(
        {
          typescript: require("typescript"),
          objectHashIgnoreUnknownHack: true,

          tsconfigOverride: {
            baseUrl:path.resolve(__dirname, `../src/m-ui/components/${pckName}/`),
            compilerOptions: {
              // 打包单个组件的时候不生成ts声明文件
              declaration: false,
              declarationDir:"./"
            },
            include:[
              path.resolve(__dirname, `../src/m-ui/components/${pckName}/`)
            ]
          },
        }
      ),
      vue({
        // target: "browser",
      })
    ],
    external(id) {
      // 对vue本身 和 自己写的包 都排除掉不打包
      return /^vue/.test(id) || /^@m-ui/.test(id);
    }
  };
});